var chatheadnya = document.getElementsByClassName('chat-head');
var chatbodynya = document.getElementsByClassName('chat-body');

$("chatheadnya").click(function(){
        $("chatbodynya").toggle();

});

var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
  	print.value = null;
    erase = false;
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (erase === true) {
        print.value = x;
        erase = false;
  }else if (x === 'sin' || x === 'tan') {
        print.value = Math.round(evil('Math.'+x+'(Math.radians('+evil(print.value)+'))') * 10000) / 10000;
        erase = true;
    } else if (x === 'log') {
        print.value = Math.round((Math.log(evil(print.value)) * 10000) / 10000);
        erase = true;
  }else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

//select theme

function changeTheme(x){
    $('body').css({"backgroundColor": x['bcgColor']});
    $('.text-center').css({"color": x['fontColor']});
}

$(document).ready(function() {
    $('.my-select').select2();

    themes = [ {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"} ];

    localStorage.setItem("themes", JSON.stringify(themes));
    var themes = JSON.parse(localStorage.getItem('themes'));

    mySelect.select2({
        'data': JSON.parse(localStorage.getItem("themes")
    )
      });

    var arraysOfTheme = JSON.parse(localStorage.getItem("themes"));
    var indigoTheme = arraysOfTheme[0]; //defaultTheme
    var defaultTheme = indigoTheme;
    var selectedTheme = defaultTheme;
    var changeTheme = defaultTheme;
    if (localStorage.getItem("selectedTheme") !== null) {
        changeTheme = JSON.parse(localStorage.getItem("selectedTheme"));
    }
    selectedTheme = changeTheme;
    changeTheme(selectedTheme);
    $('.apply-button').on('click', function () {
        var idThemeChoosed = mySelect.val();
        if (idThemeChoosed < arraysOfTheme.length) {
            selectedTheme = arraysOfTheme[idThemeChoosed];
        }
        changeTheme(idThemeChoosed);
        localStorage.setItem("selectedTheme", JSON.stringify(selectedTheme));
    })
    var cls = "msg-receive";
    $('#msg-text-area').keypress(function (e) {
        if (e.which == 13) {
            var msg = $('#msg-text-area').val();
            var old = $('.msg-insert').html();
            if (msg.length === 0) {
                alert("Message kosong");
            }
            else {
                cls = (cls == "msg-send") ? "msg-receive" : "msg-send"
                $('.msg-insert').html(old + '<p class=' + cls + '>' + msg + '</p>')
                $('#msg-text-area').val("");

            }
        }
    });
    $('#msg-text-area').keyup(function (e) {
        if (e.which == 13) {
            $('#msg-text-area').val("");
        }
    });


});



